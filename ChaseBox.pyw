import sys, math, random, time
from PyQt5 import Qt, QtCore, QtGui, QtWidgets
from OpenGL import GL

#TODO:
#BoxChase goals:
    #Space to restart
    #Messages in status bar
    #Text rendering
    #Save high scores to file
    #display high score
    #1/10 set chance of Green life goals (force 1 in ten--roll dice every 10 scores.)
    #Allow more than 3 lives if earned
    #Win at 100 scores
    #Disappear animation (also used at death of player or enemy
    #Package for Windows, Linux and Mac 32-bit and 64-bit)
    #At this point, you are very close to Vyce. Add levels, power-ups, an editor and a story, and you've got the green light! (for steam?)

class mainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        self.statusBar().showMessage('Press space to begin')
        self.setGeometry(200, 200, 640, 480)
        self.mainLayout = QtWidgets.QHBoxLayout()
        self.mainLayoutContainer = QtWidgets.QFrame()
        self.mainLayoutContainer.setLayout(self.mainLayout)
        self.setCentralWidget(self.mainLayoutContainer)
        self.glQWindowWidget = Qt.QWidget.createWindowContainer(glWindow, None, Qt.Qt.Widget)
        self.mainLayout.addWidget(self.glQWindowWidget)
        self.glQWindowWidget.grabKeyboard()
        self.glQWindowWidget.setFocus()
        #glQWindowWidget.grabMouse()

    def get_height(self):
        return(self.glQWindowWidget.height())
    def get_width(self):
        return(self.glQWindowWidget.width())

class glWindow(Qt.QWindow):
    def __init__(self):
        super().__init__()
        glformat = Qt.QSurfaceFormat()
        glformat.setVersion(3, 3)
        glformat.setProfile(Qt.QSurfaceFormat.CompatibilityProfile)
        glformat.setStereo(False)
        glformat.setSwapBehavior(Qt.QSurfaceFormat.DoubleBuffer)
        self.setSurfaceType(Qt.QWindow.OpenGLSurface)
        self.context = Qt.QOpenGLContext()
        self.context.setFormat(glformat)
        if not self.context.create():
            raise Exception('self.context.create() failed')
        self.create()

        GL.glMatrixMode(GL.GL_PROJECTION)
        GL.glLoadIdentity()
        GL.glOrtho(-1, 1, 1, -1, -1, 1)

        self.frame = 60
        self.ms = round(1000/self.frame)
        self.exit = False
        #self.mycoor= (-.5, -.5, .5, .5)
        self.sec = 0
        self.fps = 0

        self.resetGame()

    def resetGame(self):
        self.currentKeys = []
        self.pause=True
        self.firstPausePress=True
        self.bg=[0.4,0.5,0.4,0]
        self.player=[-.05, .05, -.85, -0.75, .5, .5, .5]
        self.enemy=[-.05, .05, .75, .85, .8, .5, .5, 2]
        self.prize=[-.05, .05, -.05, .05, .5, .5, .8, 3]
        self.score = 0
        self.playerSpeed = 0.01
        self.enemySpeed = 0.005
        self.angry = 0
        self.fail = 0
        self.touched = False

    def keyPressEvent(self, event):
        if event.isAutoRepeat() == False:
            print(event.key())
            if event.key() == 16777235:
                self.currentKeys.append('UP')
            if event.key() == 16777237:
                self.currentKeys.append('DOWN')
            if event.key() == 32:
                self.currentKeys.append('JUMP')
                #self.player[2] += 0.1
                #self.player[3] += 0.1
                pass
            if event.key() == 16777236:
                self.currentKeys.append('RIGHT')
            if event.key() == 16777234:
                self.currentKeys.append('LEFT')
            if event.key() == 87:
                self.currentKeys.append('W')
            if event.key() == 65:
                self.currentKeys.append('A')
            if event.key() == 83:
                self.currentKeys.append('S')
            if event.key() == 68:
                self.currentKeys.append('D')
            if event.key() == 16777216:
                sys.exit()
    def keyReleaseEvent(self, event):
        if event.isAutoRepeat() == False:
            if event.key() == 16777235:
                self.currentKeys.remove('UP')
            if event.key() == 16777237:
                self.currentKeys.remove('DOWN')
            if event.key() == 32:
                self.currentKeys.remove('JUMP')
                #self.player[2] -= 0.1
                #self.player[3] -= 0.1
                pass
            if event.key() == 16777236:
                self.currentKeys.remove('RIGHT')
            if event.key() == 16777234:
                self.currentKeys.remove('LEFT')
            if event.key() == 87:
                self.currentKeys.remove('W')
            if event.key() == 65:
                self.currentKeys.remove('A')
            if event.key() == 83:
                self.currentKeys.remove('S')
            if event.key() == 68:
                self.currentKeys.remove('D')

    def processActions(self, *keys):
        if 'UP' in self.currentKeys:
            #self.player[1] += self.playerSpeed
            #self.player[3] += self.playerSpeed
            pass
        if 'DOWN' in self.currentKeys:
            #if self.player[1] - self.player[0] > 0.05:
                #self.player[1] -= self.playerSpeed
                #self.player[3] -= self.playerSpeed
                pass
        if 'JUMP' in self.currentKeys:
            if self.firstPausePress == True:
                self.firstPausePress = False
                if self.pause == False:
                    self.pause = True
                else:
                    self.pause = False
        else:
            if self.firstPausePress == False:
                self.firstPausePress = True
        if 'RIGHT' in self.currentKeys:
            if self.collideRight == False:
                #self.player[0] += self.playerSpeed
                #self.player[1] += self.playerSpeed
                pass
        if 'LEFT' in self.currentKeys:
            if self.collideLeft == False:
                #self.player[0] -= self.playerSpeed
                #self.player[1] -= self.playerSpeed
                pass
        if 'W' in self.currentKeys:
            if self.collideTop == False:
                self.move(self.player, 'up', self.playerSpeed)
        if 'S' in self.currentKeys:
            if self.collideBottom == False:
                self.move(self.player, 'down', self.playerSpeed)
        if 'A' in self.currentKeys:
            if self.collideLeft == False:
                self.move(self.player, 'left', self.playerSpeed)
        if 'D' in self.currentKeys:
            if self.collideRight == False:
                self.move(self.player, 'right', self.playerSpeed)

        enemyActive = True
        if enemyActive == True:
            if self.pause == False:
                if self.player[0] > self.enemy[0]:
                    self.enemy[0] += self.enemySpeed
                    self.enemy[1] += self.enemySpeed
                if self.player[0] < self.enemy[0]:
                    self.enemy[0] -= self.enemySpeed
                    self.enemy[1] -= self.enemySpeed
                if self.player[2] > self.enemy[2]:
                    self.enemy[2] += self.enemySpeed
                    self.enemy[3] += self.enemySpeed
                if self.player[2] < self.enemy[2]:
                    self.enemy[2] -= self.enemySpeed
                    self.enemy[3] -= self.enemySpeed

    def move(self, entity, direction, speed):
        if self.pause == True:
            pass
        else:
            if direction == 'up':
                entity[2] += speed
                entity[3] += speed
            if direction == 'down':
                entity[2] -= speed
                entity[3] -= speed
            if direction == 'left':
                entity[0] -= speed
                entity[1] -= speed
            if direction == 'right':
                entity[0] += speed
                entity[1] += speed

    def newPrize(self):
        newPrizex = random.randint(-9, 9)
        newPrizex /= 10
        self.prize[0] = newPrizex
        self.prize[1] = newPrizex + 0.1
        newPrizey = random.randint(-9, 9)
        newPrizey /= 10
        self.prize[2] = newPrizey
        self.prize[3] = newPrizey + 0.1
        self.score += 1
        #self.enemySpeed *= 1.1
        self.angry = 2
        if self.enemySpeed == 0.005:
            self.enemySpeed *= 2

    def processCollisions(self):
        self.player[4] = .5
        self.player[5] = .5
        self.player[6] = .5
        collision = False
        self.collideBottom = False
        self.collideTop = False
        self.collideLeft = False
        self.collideRight = False
        collisionType = 0
        for rect in self.scene:
            #bottom-left
            if self.player[0] <= rect[1] and self.player[0] >= rect[0] and self.player[2] >= rect[2] and self.player[2] <= rect[3]:
                collision = True
                collisionType = rect[7]
                xdif = self.player[0] - rect[1]
                xdif *= -1
                ydif = rect[3] - self.player[2]
                if collisionType != 2:
                    if xdif >= ydif:
                        self.collideBottom = True
                    else:
                        self.collideLeft = True
            #top-left
            if self.player[0] <= rect[1] and self.player[0] >= rect[0] and self.player[3] >= rect[2] and self.player[3] <= rect[3]:
                collision = True
                collisionType = rect[7]
                xdif = rect[1] - self.player[0]
                ydif = self.player[3] - rect[2]
                if collisionType != 2:
                    if xdif >= ydif:
                        self.collideTop = True
                    else:
                        self.collideLeft = True
            #top-right
            if self.player[1] <= rect[1] and self.player[1] >= rect[0] and self.player[3] >= rect[2] and self.player[3] <= rect[3]:
                collision = True
                collisionType = rect[7]
                xdif = self.player[1] - rect[0]
                ydif = self.player[3] - rect[2]
                if collisionType != 2:
                    if xdif >= ydif:
                        self.collideTop = True
                    else:
                        self.collideRight = True
            #bottom-right
            if self.player[1] <= rect[1] and self.player[1] >= rect[0] and self.player[2] >= rect[2] and self.player[2] <= rect[3]:
                collision = True
                collisionType = rect[7]
                xdif = self.player[1] - rect[0]
                ydif = rect[3] - self.player[2]
                if collisionType != 2:
                    if xdif >= ydif:
                        self.collideBottom = True
                    else:
                        self.collideRight = True
        if collision == True and collisionType == 2:
            self.player[4] = 0
            self.player[5] = 0
            self.player[6] = 0
            if self.touched != True:
                self.touched = True
                self.fail += 1
                self.bg[0] = 0.2
                self.bg[1] = 0
                self.bg[2] = 0
            if self.fail == 3:
                print('Game over! Score: ' + str(self.score))
                time.sleep(3)
                self.resetGame
        else:
            if self.touched != False:
                self.touched=False
                if self.fail == 1:
                    self.bg[0] = 0.3
                    self.bg[1] = 0.3
                    self.bg[2] = 0.15
                elif self.fail > 1:
                    self.bg[0] = 0.2
                    self.bg[1] = 0.1
                    self.bg[2] = 0.1
                else:
                    self.bg[0] = 0.4
                    self.bg[1] = 0.5
                    self.bg[2] = 0.4
        if collision == True and collisionType == 3:
            self.player[4] = .5
            self.player[5] = .5
            self.player[6] = .8
            self.newPrize()
        if collision == True and collisionType == 1:
            self.player[4] = .8
            self.player[5] = .5
            self.player[6] = .5
            self.playerSpeed = 0.004
        else:
            if self.playerSpeed != 0.01:
                self.playerSpeed = 0.01

    def drawRect(self, rect):
        GL.glBegin(GL.GL_QUADS)
        GL.glColor3f(rect[4], rect[5], rect[6])
        GL.glVertex2f(rect[0], rect[2])
        GL.glVertex2f(rect[0], rect[3])
        GL.glVertex2f(rect[1], rect[3])
        GL.glVertex2f(rect[1], rect[2])
        GL.glEnd()

    def exposeEvent(self, ev):
        ev.accept()
        if self.isExposed() and self.isVisible():
            self.update()

    def update(self):
        self.timeStart = str(Qt.QDateTime.currentMSecsSinceEpoch())[8:]
        self.exitTime = int(self.timeStart) + self.ms
        if self.timeStart[1] != self.sec:
            self.fps_total = str(self.fps)
            self.fps = 0
            self.sec = self.timeStart[1]
            if self.angry > 1:
                self.angry -= 1
            elif self.angry == 1:
                self.angry = 0
                self.enemySpeed /= 2
        else:
            self.fps += 1
            #self.status = str(self.fps_total) + ' fps, x1: ' + str(self.player[2]) + ', x2: ' + str(self.player[0]) + ', y1: ' + str(self.player[3]) + ', y2: ' + str(self.player[1])
            self.status = 'Score: ' + str(self.score) + ' | Lives: ' + str(3 - self.fail) + ' | ' + str(self.fps_total) + ' fps'
            window.statusBar().showMessage(self.status)
        self.context.makeCurrent(self)

        GL.glClearColor(self.bg[0], self.bg[1], self.bg[2], self.bg[3])
        GL.glClearDepth(1)
        GL.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT)

        viewHeight = window.get_height()
        viewWidth = window.get_width()
        widthMargin = 0
        heightMargin = 0
        if viewHeight <= viewWidth:
            widthMargin = int((viewWidth - viewHeight) / 2)
            viewSize = viewHeight
        if viewWidth < viewHeight:
            heightMargin = int((viewHeight - viewWidth) / 2)
            viewSize = viewWidth
        GL.glViewport(widthMargin, heightMargin, viewSize, viewSize)

        #Scene
        mapRect1=[-1, -0.99, -1, 1, 0.3, 0.3, 0.3, 1]
        mapRect2=[0.99, 1, -1, 1, 0.3, 0.3, 0.3, 1]
        mapRect3=[-1, 1, -1, -0.99, 0.3, 0.3, 0.3, 1]
        mapRect4=[-1, 1, 0.99, 1, 0.3, 0.3, 0.3, 1]
        mapRect5=[-0.5, 0.5, 0.5, 0.6, 1, 1, 1, 1]
        mapRect6=[-0.5, 0.5, -0.6, -0.5, 1, 1, 1, 1]
        self.scene = [mapRect1, mapRect2, mapRect3, mapRect4, mapRect5, mapRect6,  self.enemy, self.prize]
        self.processCollisions()

        for rect in self.scene:
            self.drawRect(rect)
        self.drawRect(self.player)

        self.processActions()

        app.keyboardModifiers()

        GL.glFlush()

        self.timeStop = str(Qt.QDateTime.currentMSecsSinceEpoch())[8:]
        #print(self.exitTime, self.timeStart, self.timeStop)
        self.waitTime = self.exitTime - int(self.timeStop)
        if self.waitTime >= 0:
            QtCore.QTimer.singleShot(self.waitTime, self.stop)
        try:
            self.context.swapBuffers(self)
        except:
            pass

    def stop(self):
        #print(self.waitTime)
        pass

if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    glWindow = glWindow()
    window = mainWindow()
    window.show()
    loop=QtCore.QTimer()
    loop.timeout.connect(glWindow.update)
    loop.start(0)
    sys.exit(app.exec_())
